using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using MafiaBackEnd.Models;
using MafiaBackEnd.Data;
using System.Security.Cryptography;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;

namespace MafiaBackEnd.Controllers;

[ApiController]
[Route("api/[controller]")]
public class LoginController : ControllerBase {
    private IConfiguration _config;
    private MafiaContext mafiaContext = new MafiaContext();

    public LoginController(IConfiguration config) {
        _config = config;
    }

    [AllowAnonymous]
    [HttpPost]
    public IActionResult Login([FromBody] UserLogin userLogin) {
        var user = Authenticate(userLogin);

        if (user != null) {
            var token = Generate(user);
            return Ok(token);
        }

        return NotFound("User not found");
    }

    private string Generate(User user)
    {
        var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
        var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

        var claims = new[] {
            new Claim(ClaimTypes.NameIdentifier, user.Username),
            new Claim(ClaimTypes.Email, user.Email),
        };

        var token = new JwtSecurityToken(
            _config["Jwt:Issuer"],
            _config["Jwt:Audience"],
            claims,
            expires: DateTime.Now.AddMinutes(15),
            signingCredentials: credentials
            );
        
        return new JwtSecurityTokenHandler().WriteToken(token);
    }

    private User? Authenticate(UserLogin userLogin)
    {
        var hashedPassword = UserController.HashPassword(userLogin.Password);
        var currentUser = mafiaContext.Users.AsEnumerable()
            .Where(user => user.Username == userLogin.Username && user.Password == hashedPassword)
            .FirstOrDefault(defaultValue: null);

        return currentUser;
    }
}
