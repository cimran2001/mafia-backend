using MafiaBackEnd.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using MafiaBackEnd.Models;

namespace MafiaBackEnd.Controllers;

[ApiController]
[Route("api/[controller]")]
public class RegistrationController : ControllerBase {
    private IConfiguration _config;
    private MafiaContext mafiaContext = new MafiaContext();

    public RegistrationController(IConfiguration config) {
        _config = config;
    }

    [AllowAnonymous]
    [HttpPost]
    public async Task<IActionResult> Register([FromBody] User user) {
        var newUser = mafiaContext.Users.AsEnumerable()
            .Where(user => user.Username == user.Username)
            .FirstOrDefault(defaultValue: null);
        
        if (newUser != null)
            return Forbid("User with such an username already ");
        
        newUser = new User() {
            Username = user.Username,
            Password = UserController.HashPassword(user.Password),
            Email = user.Email,
            RegistrationDate = DateTime.Today,
            LatestChangeDate = DateTime.Today,
        };
        
        mafiaContext.Users.Add(newUser);
        await mafiaContext.SaveChangesAsync();
        return Ok("Registration completed successfully.");
    }
}
