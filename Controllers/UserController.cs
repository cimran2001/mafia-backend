using Microsoft.AspNetCore.Mvc;
using MafiaBackEnd.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using System.Security.Cryptography;
using System.Text;
using MafiaBackEnd.Data;

namespace MafiaBackEnd.Controllers;

[ApiController]
[Route("api/[controller]")]
public class UserController : ControllerBase {
    private MafiaContext mafiaContext = new MafiaContext();

    [HttpGet]
    [Authorize]
    public IActionResult GetUserStats() {
        string? username = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?.Value;
        if (username == null)
            return Forbid("User not found.");
        
        var user = mafiaContext.Users.Where(u => u.Username == username).FirstOrDefault();
        if (user == null)
            return Forbid("User not found.");
        return Ok(user);
    }

    private User? GetCurrentUser() {
        var identity = HttpContext.User.Identity as ClaimsIdentity;

        if (identity == null)
            return null;
        
        var userClaims = identity.Claims;
        return new User() {
            Username = userClaims.First(u => u.Type == ClaimTypes.NameIdentifier).Value,
            Email = userClaims.First(u => u.Type == ClaimTypes.Email).Value
        };
    }

    public static string HashPassword(string password) {
        SHA512 alg = SHA512.Create();
        byte[] result = alg.ComputeHash(Encoding.UTF8.GetBytes(password));
        return Encoding.UTF8.GetString(result);
    }
}
