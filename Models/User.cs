namespace MafiaBackEnd.Models;

public class User {
    public uint Id { get; set; }
    public string Username { get; set; } = null!;
    public string Password { get; set; } = null!;
    public string Email { get; set; } = null!;
    public string? ProfilePicture_path { get; set; }
    public DateTime RegistrationDate { get; set; }
    public DateTime LatestChangeDate { get; set; }
    public string? ConfirmationCode { get; set; }
    public DateTime? ConfirmationDate { get; set; }

    public virtual ICollection<UserRoomRole>? UserRoomRoles { get; set; }
}
