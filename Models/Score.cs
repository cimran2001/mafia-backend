using System.ComponentModel.DataAnnotations.Schema;

namespace MafiaBackEnd.Models;

public class Score {
    public uint Id { get; set; }

    [ForeignKey("User")]
    public uint UserId { get; set; }
    public virtual User User { get; set; } = null!;
    
    public uint GamesPlayed { get; set; }
    public uint GamesWon { get; set; }
    public uint GamesLost { get; set; }
    public DateTime LastGameTime { get; set; }
}
