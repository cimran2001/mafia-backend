using System.ComponentModel.DataAnnotations.Schema;

namespace MafiaBackEnd.Models;

public class Room {
    public uint Id { get; set; }
    public string Name { get; set; } = null!;
    public string Status { get; set; } = null!;
    public uint Size { get; set; }
    public uint Players { get; set; }

    [ForeignKey("User")]
    public uint OwnerId { get; set; }
    public virtual User User { get; set; } = null!;

    public DateTime CreationTime { get; set; }

    public virtual ICollection<UserRoomRole>? UserRoomRoles { get; set; }
}
