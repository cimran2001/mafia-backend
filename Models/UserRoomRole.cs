using System.ComponentModel.DataAnnotations.Schema;

namespace MafiaBackEnd.Models;

public class UserRoomRole {
    [ForeignKey("User")]
    public uint UserId { get; set; }
    public virtual User User { get; set; } = null!;

    [ForeignKey("Room")]
    public uint RoomId { get; set; }
    public virtual Room Room { get; set; } = null!;

    [ForeignKey("Role")]
    public uint RoleId { get; set; }
    public virtual Role Role { get; set; } = null!;
}
