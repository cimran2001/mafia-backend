using System.ComponentModel.DataAnnotations.Schema;

namespace MafiaBackEnd.Models;

public class Session {
    public uint Id { get; set; }
    
    [ForeignKey("User")]
    public uint UserId { get; set; }
    public virtual User User { get; set; } = null!;

    public DateTime CreatedTime { get; set; }
    public DateTime ValidToTime { get; set; }
}