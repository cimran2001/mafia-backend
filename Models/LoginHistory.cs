using System.ComponentModel.DataAnnotations.Schema;

namespace MafiaBackEnd.Models;

public class LoginHistory {
    public uint Id { get; set; }

    [ForeignKey("User")]
    public uint UserId { get; set; }
    public virtual User User { get; set; } = null!;
    
    public DateTime LoginTime { get; set; }
    public DateTime LogoutTime { get; set; }
    public string LoginData { get; set; } = null!;
}
