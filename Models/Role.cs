namespace MafiaBackEnd.Models;

public class Role {
    public uint Id { get; set; }
    public String Name { get; set; } = null!;

    public virtual ICollection<UserRoomRole>? UserRoomRoles { get; set; }
}
