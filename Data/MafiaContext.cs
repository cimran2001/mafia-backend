using MafiaBackEnd.Models;
using Microsoft.EntityFrameworkCore;

namespace MafiaBackEnd.Data;

public class MafiaContext : DbContext {
    public DbSet<LoginHistory> LoginHistories { get; set; } = null!;
    public DbSet<Role> Roles { get; set; } = null!;
    public DbSet<Room> Rooms { get; set; } = null!;
    public DbSet<Score> Scores { get; set; } = null!;
    public DbSet<Session> Sessions { get; set; } = null!;
    public DbSet<User> Users { get; set; } = null!;
    public DbSet<UserRoomRole> UsersRoomsRoles { get; set; } = null!;

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

        var connectionString = configuration.GetConnectionString("MafiaDbConnection");
        optionsBuilder.UseSqlServer(connectionString);
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder) {
        modelBuilder.Entity<UserRoomRole>().HasKey(urr => new { urr.UserId, urr.RoomId, urr.RoleId });

        modelBuilder.Entity<UserRoomRole>()
            .HasOne(urr => urr.User)
            .WithMany(u => u.UserRoomRoles)
            .OnDelete(DeleteBehavior.NoAction);
        
        modelBuilder.Entity<UserRoomRole>()
            .HasOne(urr => urr.Room)
            .WithMany(r => r.UserRoomRoles)
            .OnDelete(DeleteBehavior.NoAction);
        
        modelBuilder.Entity<UserRoomRole>()
            .HasOne(urr => urr.Role)
            .WithMany(r => r.UserRoomRoles)
            .OnDelete(DeleteBehavior.NoAction);
    }
}